from os import device_encoding
import ssl
import re
import sys
import json
import requests
import urllib3
import argparse
import concurrent
from itertools import repeat
from concurrent.futures import ThreadPoolExecutor

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


URL_POST_DEVICE_SET_LOG            = 'https://%s:9182/api/config/devices/device/%s/config/orgs/org-services/%s/security/access-policies/access-policy-group/%s/rules/access-policy/%s/set/lef/event'
URL_POST_DEVICE_SET_TAG            = 'https://%s:9182/api/config/devices/device/%s/config/orgs/org-services/%s/security/access-policies/access-policy-group/%s/rules/access-policy/%s/tag'
URL_POST_DEVICE_DEL_TAG            = 'https://%s:9182/api/config/devices/device/%s/config/orgs/org-services/%s/security/access-policies/access-policy-group/%s/rules/access-policy/%s/tag'
URL_POST_DEVICE_SET_LOG_PROFILE    = 'https://%s:9182/api/config/devices/device/%s/config/orgs/org-services/%s/security/access-policies/access-policy-group/%s/rules/access-policy/%s/set/lef/profile'
URL_POST_DEVICE_SET_DISABLE        = 'https://%s:9182/api/config/devices/device/%s/config/orgs/org-services/%s/security/access-policies/access-policy-group/%s/rules/access-policy/%s/rule-disable'
URL_DEL_DEVICE_DISABLE             = 'https://%s:9182/api/config/devices/device/%s/config/orgs/org-services/%s/security/access-policies/access-policy-group/%s/rules/access-policy/%s/rule-disable'
URL_DEL_DEVICE_RULE                = 'https://%s:9182/api/config/devices/device/%s/config/orgs/org-services/%s/security/access-policies/access-policy-group/%s/rules/access-policy/%s'
URL_GET_DEVICE_ALL_RULES           = 'https://%s:9182/api/config/devices/device/%s/config/orgs/org-services/%s/security/access-policies/access-policy-group/%s/rules/access-policy'

URL_POST_DEV_TEMPLATE_SET_LOG          = 'https://%s:9182/api/config/devices/template/%s/config/orgs/org-services/%s/security/access-policies/access-policy-group/%s/rules/access-policy/%s/set/lef/event'
URL_POST_DEV_TEMPLATE_SET_TAG          = 'https://%s:9182/api/config/devices/template/%s/config/orgs/org-services/%s/security/access-policies/access-policy-group/%s/rules/access-policy/%s/tag'
URL_POST_DEV_TEMPLATE_DEL_TAG          = 'https://%s:9182/api/config/devices/template/%s/config/orgs/org-services/%s/security/access-policies/access-policy-group/%s/rules/access-policy/%s/tag'
URL_POST_DEV_TEMPLATE_SET_LOG_PROFILE  = 'https://%s:9182/api/config/devices/template/%s/config/orgs/org-services/%s/security/access-policies/access-policy-group/%s/rules/access-policy/%s/set/lef/profile'
URL_POST_DEV_TEMPLATE_SET_DISABLE      = 'https://%s:9182/api/config/devices/template/%s/config/orgs/org-services/%s/security/access-policies/access-policy-group/%s/rules/access-policy/%s/rule-disable'
URL_DEL_DEV_TEMPLATE_DISABLE           = 'https://%s:9182/api/config/devices/template/%s/config/orgs/org-services/%s/security/access-policies/access-policy-group/%s/rules/access-policy/%s/rule-disable'
URL_DEL_DEV_TEMPLATE_RULE              = 'https://%s:9182/api/config/devices/template/%s/config/orgs/org-services/%s/security/access-policies/access-policy-group/%s/rules/access-policy/%s'
URL_GET_DEV_TEMPLATE_ALL_RULES         = 'https://%s:9182/api/config/devices/template/%s/config/orgs/org-services/%s/security/access-policies/access-policy-group/%s/rules/access-policy'

URL_GET_ALL_TENANT_DEV_TEMPLATE        = 'https://%s:9182/vnms/template/metadata?organization=%s'
URL_GET_ALL_TENANT_DEVICES             = 'https://%s:9182/vnms/appliance/filter/%s?offset=0&limit=25'


#TO BE CHANGE in the MAIN CODE 
HTTP_HEADER                 = {"Content-Type": "application/json", "Accept": "application/json"}
PAYLOAD_EVENT_END           = json.dumps({ "event" : "end" })
PAYLOAD_LOG_PROFILE_DEFAULT = json.dumps({ "profile": "Default-Logging-Profile" })
PAYLOAD_LOG_PROFILE_NONE    = json.dumps({ "profile": "" })
PAYLOAD_RULE_DISABLE        = json.dumps({ "rule-disable": "true" })
PAYLOAD_RULE_ENABLE         = json.dumps({ "rule-disable": "false" })

#MESSAGE
RUN_MESSAGE_HEADER          = '\n[ %s ]'
GET_POLICY_MESSAGE_ERROR    = '!!! Could not retrieve Rules on %s'

def help_filter():
    print()
    print('has.no.log           : Match rules with no log settings')
    print('has.log              : Match rules with log settings')
    print('has.no.log.profile   : Match rules with no log-profile settings')
    print('has.log.profile      : Match rules if a log-profile exist')
    print('has.tag:XXX          : Match rules with the tag value XXX')
    print('has.action.allow     : Match rules with the action allow')
    print('has.zone.src:XXX     : Match rules with the source zone XXX')
    print('has.zone.dst:XXX     : Match rules with the destination zone XXX')
    print('has.add.src:XXX      : Match rules with the source address or address-group XXX')
    print('has.add.dst:XXX      : Match rules with the destination address or address-group XXX')
    print('has.app:XXX          : Match rules with the application or application-group XXX')
    print('has.service:XXX      : Match rules with the service XXX')
    print('is.enable            : Match enabled rules')
    print('is.disable           : Match disabled rules')
    print('none                 : Match any rules')
    print()
    exit()

def help_action():
    print()
    print('display           : Display the rule name')
    print('delete            : Delete the rule')
    print('set.log           : Set traffic logging on the rule ')
    print('set.log.profile   : Set a traffic log profile on the rule')
    print('set.rule.disable  : Disable the rule')
    print('set.rule.enable   : Enable the rule')
    print('set.tag:XXX       : Add the tag XXX to the rule')
    print('del.tag:XXX       : Delete the tag XXX from the rule')
    print()
    exit()

def print_api_error():
    print('')
    print('-> There is a problem reaching the API. Please verify your Credentials and:')
    print('--> The URL: '+ args.director)
    print('--> The Organization Name: '+ args.org)
    print('--> The Device Name: '+ args.target)
    print('--> The Policy Group Name: '+ args.group)
    print('')      
    print('-> Use the help menu if needed:')
    print('--> python3 rules-edit.py -h')
    print('--> python3 rules-edit.py --action help')
    print('--> python3 rules-edit.py --filter help')
    print('')
    exit() 

def filter_match_rule(rule_json , filter):
    if filter == 'has.no.log' and rule_json['set']['lef']['event'] == 'never':
        return True
    elif filter == 'has.log' and rule_json['set']['lef']['event'] != 'never':
        return True
    elif filter == 'has.no.log.profile' and 'profile' not in rule_json['set']['lef']:
        return True
    elif filter == 'has.log.profile' and 'profile' in rule_json['set']['lef']:
        return True
    elif filter == 'is.enable' and str(rule_json['rule-disable']) == 'false':
        return True
    elif filter == 'is.disable' and str(rule_json['rule-disable']) == 'true':
        return True
    elif filter == 'has.action.allow' and str(rule_json['set']['action']) == 'allow':
        return True

    elif re.match('has.tag\:.*',filter) and 'tag' in rule_json:
        for tag_value in rule_json['tag']:
            if tag_value == filter.split(':',1)[1]:
                return True

    elif re.match('has.add.src\:.*',filter) and 'source' in rule_json['match']:
        if 'address' in rule_json['match']['source']:
            if 'address-list' in rule_json['match']['source']['address']:
                for address_value in rule_json['match']['source']['address']['address-list']:
                    if address_value == filter.split(':',1)[1]:
                        return True
            if 'address-group-list' in rule_json['match']['source']['address']:
                for address_group_value in rule_json['match']['source']['address']['address-group-list']:
                    if address_group_value == filter.split(':',1)[1]:
                        return True

    elif re.match('has.add.dst\:.*',filter) and 'destination' in rule_json['match']:
        if 'address' in rule_json['match']['destination']:
            if 'address-list' in rule_json['match']['destination']['address']:
                for address_value in rule_json['match']['destination']['address']['address-list']:
                    if address_value == filter.split(':',1)[1]:
                        return True
            if 'address-group-list' in rule_json['match']['destination']['address']:
                for address_group_value in rule_json['match']['destination']['address']['address-group-list']:
                    if address_group_value == filter.split(':',1)[1]:
                        return True

    elif re.match('has.zone.src\:.*',filter) and 'source' in rule_json['match']:
        if 'zone' in rule_json['match']['source']:
            for zone_value in rule_json['match']['source']['zone']['zone-list']:
                if zone_value == filter.split(':',1)[1]:
                    return True

    elif re.match('has.zone.dst\:.*',filter) and 'destination' in rule_json['match']:
        if 'zone' in rule_json['match']['destination']:
            for zone_value in rule_json['match']['destination']['zone']['zone-list']:
                if zone_value == filter.split(':',1)[1]:
                    return True
                
    elif re.match('has.app\:.*',filter) and 'application' in rule_json['match']:
        if 'predefined-application-list' in rule_json['match']['application']:
            for app_value in rule_json['match']['application']['predefined-application-list']:
                if app_value == filter.split(':',1)[1]:
                    return True
        if 'predefined-group-list' in rule_json['match']['application']:
            for app_value in rule_json['match']['application']['predefined-group-list']:
                if app_value == filter.split(':',1)[1]:
                    return True
                
        if 'predefined-filter-list' in rule_json['match']['application']:
            for app_value in rule_json['match']['application']['predefined-filter-list']:
                if app_value == filter.split(':',1)[1]:
                    return True
                
    elif re.match('has.service\:.*',filter) and 'services' in rule_json['match']:
        if 'predefined-services-list' in rule_json['match']['services']:
            for service_value in rule_json['match']['services']['predefined-services-list']:
                if service_value == filter.split(':',1)[1]:
                    return True
        if 'services-list' in rule_json['match']['services']:
            for service_value in rule_json['match']['services']['services-list']:
                if service_value == filter.split(':',1)[1]:
                    return True

    elif filter == 'none':
        return True
    
    if not re.match('has.zone.dst\:.*',filter) and not re.match('has.zone.src\:.*',filter) and not re.match('has.tag\:.*',filter) and filter != 'has.no.log' and filter != 'has.log' and filter != 'has.no.log.profile' and filter != 'has.log.profile' and filter != 'is.enable' and filter != 'is.disable' and filter != 'has.action.allow' and not re.match('has.app\:.*',filter) and not re.match('has.service\:.*',filter) and not re.match('has.add.src\:.*',filter) and not re.match('has.add.dst\:.*',filter):
        print()
        print('The filter is incorrect. Use --filter help for details on how to use filters')
        print()
        exit()


def exec_action(target_name, rule_json , filter):

    if args.action == "display" and filter_match_rule(rule_json , filter):
        display_rule_name(rule_json['name'])

    elif args.action == "set.log" and filter_match_rule(rule_json , filter):
        set_log(target_name, rule_json['name'])

    elif args.action == "set.log.profile" and filter_match_rule(rule_json , filter):
        set_log_profile(target_name, rule_json['name'])

    elif args.action == "set.rule.disable" and filter_match_rule(rule_json , filter):
        disable_rule(target_name, rule_json['name'])

    elif args.action == "set.rule.enable" and filter_match_rule(rule_json , filter):
        enable_rule(target_name, rule_json['name'])

    elif args.action == "delete" and filter_match_rule(rule_json , filter):
        delete_rule(target_name, rule_json['name'])

    elif re.match('set.tag\:.*',args.action) and filter_match_rule(rule_json , filter):

        tag_to_add = args.action.split(':',1)[1]

        if not 'tag' in rule_json:
            set_tag(target_name, rule_json['name'], tag_to_add, tag_to_add)
        else:
            rule_json['tag'].append(tag_to_add)
            set_tag(target_name, rule_json['name'],rule_json['tag'],tag_to_add)

    elif re.match('del.tag\:.*',args.action) and filter_match_rule(rule_json , filter):

        tag_to_remove = args.action.split(':',1)[1]

        if not 'tag' in rule_json:
            return
        elif tag_to_remove in rule_json['tag']:
            rule_json['tag'].remove(tag_to_remove)
            delete_tag(target_name, rule_json['name'],rule_json['tag'],tag_to_remove)
            
    if not re.match('del.tag\:.*',args.action) and not re.match('set.tag\:.*',args.action) and args.action != "display" and args.action != "set.log" and args.action != "set.log.profile" and args.action != "set.rule.disable" and args.action != "set.rule.enable" and args.action != "delete":
        print()
        print('The action is incorrect. Use --action help for details on how to use filters')
        print()
        exit()

def display_rule_name(rule_name):
    print("--> "+rule_name)

def set_log(target_name, rule_name):
    if re.match('device\:.*',args.target):
        url_log     = URL_POST_DEVICE_SET_LOG % (args.director, target_name, args.org, args.group, rule_name)
        api_request = requests.put(url_log, auth=(args.user, args.password), headers=HTTP_HEADER, data=payload_event, verify=False)
        print("--> setting log on rule "+rule_name)
    if re.match('template\:.*',args.target):
        url_log     = URL_POST_DEV_TEMPLATE_SET_LOG % (args.director, target_name, args.org, args.group, rule_name)
        api_request = requests.put(url_log, auth=(args.user, args.password), headers=HTTP_HEADER, data=payload_event, verify=False)
        print("--> setting log on rule "+rule_name)

def set_tag(target_name, rule_name,tag_list,tag_to_add):
    if re.match('device\:.*',args.target):
        url_tag     = URL_POST_DEVICE_SET_TAG % (args.director, target_name, args.org, args.group, rule_name)
        api_request = requests.put(url_tag, auth=(args.user, args.password), headers=HTTP_HEADER, data=json.dumps({ "tag": tag_list }), verify=False)
        print("--> setting tag "+tag_to_add+" on rule "+rule_name)
    if re.match('template\:.*',args.target):
        url_tag     = URL_POST_DEV_TEMPLATE_SET_TAG % (args.director, target_name, args.org, args.group, rule_name)
        api_request = requests.put(url_tag, auth=(args.user, args.password), headers=HTTP_HEADER, data=json.dumps({ "tag": tag_list }), verify=False)
        print("--> setting tag "+tag_to_add+" on rule "+rule_name)

def delete_tag(target_name, rule_name,tag_list,tag_to_remove):
    if re.match('device\:.*',args.target):
        url_tag     = URL_POST_DEVICE_DEL_TAG % (args.director, target_name, args.org, args.group, rule_name)
        api_request = requests.put(url_tag, auth=(args.user, args.password), headers=HTTP_HEADER, data=json.dumps({ "tag": tag_list }), verify=False)
        print("--> removing tag "+tag_to_remove+" on rule "+rule_name)
    if re.match('template\:.*',args.target):
        url_tag     = URL_POST_DEV_TEMPLATE_DEL_TAG % (args.director, target_name, args.org, args.group, rule_name)
        api_request = requests.put(url_tag, auth=(args.user, args.password), headers=HTTP_HEADER, data=json.dumps({ "tag": tag_list }), verify=False)
        print("--> removing tag "+tag_to_remove+" on rule "+rule_name)

def set_log_profile(target_name, rule_name):
    if re.match('device\:.*',args.target):
        url_profile     = URL_POST_DEVICE_SET_LOG_PROFILE % (args.director, target_name, args.org, args.group, rule_name)
        api_request     = requests.put(url_profile, auth=(args.user, args.password), headers=HTTP_HEADER, data=payload_log_profile, verify=False)
        print("--> setting log profile on "+rule_name)
    if re.match('template\:.*',args.target):
        url_profile     = URL_POST_DEV_TEMPLATE_SET_LOG_PROFILE % (args.director, target_name, args.org, args.group, rule_name)
        api_request     = requests.put(url_profile, auth=(args.user, args.password), headers=HTTP_HEADER, data=payload_log_profile, verify=False)
        print("--> setting log profile on "+rule_name)

def disable_rule(target_name, rule_name):
    if re.match('device\:.*',args.target):
        url_disable     = URL_POST_DEVICE_SET_DISABLE % (args.director, target_name, args.org, args.group, rule_name)
        api_request     = requests.put(url_disable, auth=(args.user, args.password), headers=HTTP_HEADER, data=payload_rule_disable, verify=False)
        print("--> disabling rule "+rule_name)
    if re.match('template\:.*',args.target):
        url_disable     = URL_POST_DEV_TEMPLATE_SET_DISABLE % (args.director, target_name, args.org, args.group, rule_name)
        api_request     = requests.put(url_disable, auth=(args.user, args.password), headers=HTTP_HEADER, data=payload_rule_disable, verify=False)
        print("--> disabling rule "+rule_name)

def enable_rule(target_name, rule_name):
    if re.match('device\:.*',args.target):
        url_enable      = URL_DEL_DEVICE_DISABLE % (args.director, target_name, args.org, args.group, rule_name)
        api_request     = requests.put(url_enable, auth=(args.user, args.password), headers=HTTP_HEADER, data=payload_rule_enable, verify=False)
        print("--> enabling rule "+rule_name)
    if re.match('template\:.*',args.target):
        url_enable      = URL_DEL_DEV_TEMPLATE_DISABLE % (args.director, target_name, args.org, args.group, rule_name)
        api_request     = requests.put(url_enable, auth=(args.user, args.password), headers=HTTP_HEADER, data=payload_rule_enable, verify=False)
        print("--> enabling rule "+rule_name)

def delete_rule(target_name, rule_name):
    if re.match('device\:.*',args.target):
        url_delete      = URL_DEL_DEVICE_RULE % (args.director, target_name, args.org, args.group, rule_name)
        api_request     = requests.delete(url_delete, auth=(args.user, args.password), headers=HTTP_HEADER, verify=False)
        print("--> deleting rule "+rule_name)
    if re.match('template\:.*',args.target):
        url_delete      = URL_DEL_DEV_TEMPLATE_RULE % (args.director, target_name, args.org, args.group, rule_name)
        api_request     = requests.delete(url_delete, auth=(args.user, args.password), headers=HTTP_HEADER, data=payload_rule_enable, verify=False)
        print("--> deleting rule "+rule_name)

def analyze_response_code(reponse_code , api_response):
    if not re.match('2\d\d',str(reponse_code)):
        print('')
        print('-> There is a problem with an API request')
        print('-> '+str(api_response))
        print('')
        exit()

def get_tenant_dev_template_name_list():
  url_get_template      = URL_GET_ALL_TENANT_DEV_TEMPLATE % (args.director, args.org)
  api_request           = requests.get(url_get_template, auth=(args.user, args.password), headers=HTTP_HEADER, verify=False)
  template_json         = api_request.json()["versanms.templates"]
  template_list         = []

  for template in template_json["template"]:
    template_list.append(template["name"])

  return template_list

def get_tenant_device_list():
  url_get_devices       = URL_GET_ALL_TENANT_DEVICES % (args.director, args.org)
  api_request           = requests.get(url_get_devices, auth=(args.user, args.password), headers=HTTP_HEADER, verify=False)
  devise_json           = api_request.json()
  gw_name_list          = []

  for gateway in devise_json["versanms.ApplianceStatusResult"]["appliances"]:
    gw_name_list.append(gateway["name"])

  return gw_name_list

def get_security_rules(target_url, target):
    try:
        api_request     = requests.get(target_url, auth=(args.user, args.password), headers=HTTP_HEADER, verify=False)
        api_response    = api_request.text
        reponse_code    = api_request.status_code
        json_output     = json.loads(api_response)

        return json_output

    except:
        print(GET_POLICY_MESSAGE_ERROR % (target) )


if __name__ == '__main__':

    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter, description='Script to manage Security Rules on VOS CPE and Templates')
    parser.add_argument('--director', default='director.yourcompany.com', type=str,help='IP address or FQDN of Versa Director')
    parser.add_argument('--target', default='', type=str,help='Device (device:XXX) or Device Template (template:XXX) where the changes must be applied.')
    parser.add_argument('--org', default='Versa', type=str,help='Organization name')
    parser.add_argument('--group', default='Default-Policy', type=str,help='Policy Group Name')
    parser.add_argument('--user', default='Administrator', type=str,help='GUI username of Director')
    parser.add_argument('--password', default='password', type=str,help='GUI password of Director')
    parser.add_argument('--action', default='display', type=str,help='Action to be taken on the selected rules. Use --action help for details')
    parser.add_argument('--filter', default='none', type=str,help='Filter to be applied to limit the scope of the action. Use --filter help for details')

    args = parser.parse_args()

    if args.filter == 'help':
        help_filter()

    if args.action == 'help':
        help_action()

    payload_event           = json.dumps({ "event" : "end" })
    payload_log_profile     = json.dumps({ "profile": "Default-Logging-Profile" })
    payload_no_log_profile  = json.dumps({ "profile": "" })
    payload_rule_disable    = json.dumps({ "rule-disable": "true" })
    payload_rule_enable     = json.dumps({ "rule-disable": "false" })

    filter                  = args.filter

    if re.match('device\:all',args.target):
        all_devices = get_tenant_device_list()

        for device_name in all_devices:

            target_name = device_name
            print(RUN_MESSAGE_HEADER % (device_name))
            try:
                url_all_rules   = URL_GET_DEVICE_ALL_RULES % (args.director, device_name, args.org, args.group)
                device_rules     = get_security_rules(url_all_rules,device_name)
                for rule in device_rules['access-policy']:
                    exec_action(target_name, rule,filter)
            except:
                pass
    
    elif re.match('template\:all',args.target):
        all_templates = get_tenant_dev_template_name_list()

        for template_name in all_templates:

            target_name = template_name
            print(RUN_MESSAGE_HEADER % (template_name))
            try:
                url_all_rules   = URL_GET_DEV_TEMPLATE_ALL_RULES % (args.director, template_name, args.org, args.group)    
                template_rules  = get_security_rules(url_all_rules, template_name)

                for rule in template_rules['access-policy']:
                    exec_action(target_name, rule,filter)
            except:
                pass

    elif re.match('device\:.*',args.target):
        target_name = args.target.split(':',1)[1]
        print(RUN_MESSAGE_HEADER % (target_name))

        url_all_rules   = URL_GET_DEVICE_ALL_RULES % (args.director, target_name, args.org, args.group)

        try:
            url_all_rules   = URL_GET_DEVICE_ALL_RULES % (args.director, target_name, args.org, args.group)
            device_rules     = get_security_rules(url_all_rules, target_name)
            for rule in device_rules['access-policy']:
                exec_action(target_name, rule,filter)
        except:
            pass

    elif re.match('template\:.*',args.target):
        target_name = args.target.split(':',1)[1]
        print(RUN_MESSAGE_HEADER % (target_name))

        url_all_rules   = URL_GET_DEV_TEMPLATE_ALL_RULES % (args.director, target_name, args.org, args.group)   

        try:
            url_all_rules   = URL_GET_DEV_TEMPLATE_ALL_RULES % (args.director, target_name, args.org, args.group)    
            template_rules  = get_security_rules(url_all_rules,target_name)

            for rule in template_rules['access-policy']:
                exec_action(target_name, rule,filter)
        except:
            pass


