from os import device_encoding
import ssl
import re
import sys
import json
import requests
import urllib3
import argparse
import concurrent
from itertools import repeat
from concurrent.futures import ThreadPoolExecutor

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

URL_POST_DEVICE_SET_TAG            = 'https://%s:9182/api/config/devices/device/%s/config/orgs/org-services/%s/security/access-policies/access-policy-group/%s/rules/access-policy/%s/tag'
URL_POST_DEVICE_SET_DISABLE        = 'https://%s:9182/api/config/devices/device/%s/config/orgs/org-services/%s/security/access-policies/access-policy-group/%s/rules/access-policy/%s/rule-disable'
URL_DEL_DEVICE_RULE                = 'https://%s:9182/api/config/devices/device/%s/config/orgs/org-services/%s/security/access-policies/access-policy-group/%s/rules/access-policy/%s'
URL_GET_DEVICE_ALL_RULES           = 'https://%s:9182/api/config/devices/device/%s/config/orgs/org-services/%s/security/access-policies/access-policy-group/%s/rules/access-policy'

URL_POST_DEV_TEMPLATE_SET_TAG          = 'https://%s:9182/api/config/devices/template/%s/config/orgs/org-services/%s/security/access-policies/access-policy-group/%s/rules/access-policy/%s/tag'
URL_POST_DEV_TEMPLATE_SET_DISABLE      = 'https://%s:9182/api/config/devices/template/%s/config/orgs/org-services/%s/security/access-policies/access-policy-group/%s/rules/access-policy/%s/rule-disable'
URL_DEL_DEV_TEMPLATE_RULE              = 'https://%s:9182/api/config/devices/template/%s/config/orgs/org-services/%s/security/access-policies/access-policy-group/%s/rules/access-policy/%s'
URL_GET_DEV_TEMPLATE_ALL_RULES         = 'https://%s:9182/api/config/devices/template/%s/config/orgs/org-services/%s/security/access-policies/access-policy-group/%s/rules/access-policy'

URL_GET_ALL_TENANT_DEV_TEMPLATE        = 'https://%s:9182/vnms/template/metadata?organization=%s'
URL_GET_ALL_TENANT_DEVICES             = 'https://%s:9182/vnms/appliance/filter/%s?offset=0&limit=25'

HTTP_HEADER                 = {"Content-Type": "application/json", "Accept": "application/json"}
PAYLOAD_RULE_DISABLE        = json.dumps({ "rule-disable": "true" })
RUN_MESSAGE_HEADER          = '\n[ %s ]'
GET_POLICY_MESSAGE_ERROR    = '!!! Could not retrieve Rules on %s'
TAG_DEFAULT_VALUE           = 'SHADOWBY:%s'

MSG_SHADOWING_DISPLAY       = '--> Shadowing detected with rule %s. Tagging rule %s'
MSG_SHADOWING_DISABLE       = '--> Shadowing detected with rule %s. Disabling rule %s'
MSG_SHADOWING_DELETE        = '--> Shadowing detected with rule %s. Deleting rule %s'
MSG_SHADOWING_TAG        = '--> Shadowing detected with rule %s. Tagging rule %s'

def help_action():
    print()
    print('display           : Display the shadowed rules')
    print('delete            : Delete the shadowed rules')
    print('disable           : Disable the shadowed rules')
    print('tag               : Tag the shadowed rule. The TAG value include the shadowing rule name')
    print()
    exit()

def exec_action(target_name, primary_rule, candidate_rule):

    if args.action == "display":
        display_rule_name(primary_rule['name'],candidate_rule['name'] )

    elif args.action == "disable":
        disable_rule(target_name, primary_rule['name'], candidate_rule['name'])

    elif args.action == "delete":
        delete_rule(target_name, primary_rule['name'], candidate_rule['name'])

    elif re.match('tag',args.action):

        tag_to_add = TAG_DEFAULT_VALUE % (primary_rule['name'])

        if not 'tag' in candidate_rule:
            set_tag(target_name, primary_rule['name'], candidate_rule['name'], tag_to_add, tag_to_add)
        else:
            candidate_rule['tag'].append(tag_to_add)
            set_tag(target_name, primary_rule['name'], candidate_rule['name'],candidate_rule['tag'],tag_to_add)
            
    if  args.action != "tag" and args.action != "display" and args.action != "disable"  and args.action != "delete":
        print()
        print('The action is incorrect. Use --action help for details on how to use filters')
        print()
        exit()

def display_rule_name(primary_rule_name, candidate_rule_name):
    print(MSG_SHADOWING_DISPLAY % (primary_rule_name, candidate_rule_name) )

def set_tag(target_name, primary_rule_name, candidate_rule_name, tag_list, tag_to_add):
    if re.match('device\:.*',args.target):
        url_tag     = URL_POST_DEVICE_SET_TAG % (args.director, target_name, args.org, args.group, candidate_rule_name)
        api_request = requests.put(url_tag, auth=(args.user, args.password), headers=HTTP_HEADER, data=json.dumps({ "tag": tag_list }), verify=False)
        print(MSG_SHADOWING_TAG % (primary_rule_name, candidate_rule_name) )
    if re.match('template\:.*',args.target):
        url_tag     = URL_POST_DEV_TEMPLATE_SET_TAG % (args.director, target_name, args.org, args.group, candidate_rule_name)
        api_request = requests.put(url_tag, auth=(args.user, args.password), headers=HTTP_HEADER, data=json.dumps({ "tag": tag_list }), verify=False)
        print(MSG_SHADOWING_TAG % (primary_rule_name, candidate_rule_name) )

def disable_rule(target_name, primary_rule_name, candidate_rule_name):
    if re.match('device\:.*',args.target):
        url_disable     = URL_POST_DEVICE_SET_DISABLE % (args.director, target_name, args.org, args.group, candidate_rule_name)
        api_request     = requests.put(url_disable, auth=(args.user, args.password), headers=HTTP_HEADER, data=PAYLOAD_RULE_DISABLE, verify=False)
        print(MSG_SHADOWING_DISABLE % (primary_rule_name, candidate_rule_name) )
    if re.match('template\:.*',args.target):
        url_disable     = URL_POST_DEV_TEMPLATE_SET_DISABLE % (args.director, target_name, args.org, args.group, candidate_rule_name)
        api_request     = requests.put(url_disable, auth=(args.user, args.password), headers=HTTP_HEADER, data=PAYLOAD_RULE_DISABLE, verify=False)
        print(MSG_SHADOWING_DISABLE % (primary_rule_name, candidate_rule_name) )

def delete_rule(target_name, primary_rule_name, candidate_rule_name):
    if re.match('device\:.*',args.target):
        url_delete      = URL_DEL_DEVICE_RULE % (args.director, target_name, args.org, args.group, candidate_rule_name)
        api_request     = requests.delete(url_delete, auth=(args.user, args.password), headers=HTTP_HEADER, verify=False)
        print(MSG_SHADOWING_DELETE % (primary_rule_name, candidate_rule_name) )
    if re.match('template\:.*',args.target):
        url_delete      = URL_DEL_DEV_TEMPLATE_RULE % (args.director, target_name, args.org, args.group, candidate_rule_name)
        api_request     = requests.delete(url_delete, auth=(args.user, args.password), headers=HTTP_HEADER, verify=False)
        print(MSG_SHADOWING_DELETE % (primary_rule_name, candidate_rule_name) )

def analyze_response_code(reponse_code , api_response):
    if not re.match('2\d\d',str(reponse_code)):
        print('')
        print('-> There is a problem with an API request')
        print('-> '+str(api_response))
        print('')
        exit()

def get_tenant_dev_template_name_list():
  url_get_template      = URL_GET_ALL_TENANT_DEV_TEMPLATE % (args.director, args.org)
  api_request           = requests.get(url_get_template, auth=(args.user, args.password), headers=HTTP_HEADER, verify=False)
  template_json         = api_request.json()["versanms.templates"]
  template_list         = []

  for template in template_json["template"]:
    template_list.append(template["name"])

  return template_list

def get_tenant_device_list():
  url_get_devices       = URL_GET_ALL_TENANT_DEVICES % (args.director, args.org)
  api_request           = requests.get(url_get_devices, auth=(args.user, args.password), headers=HTTP_HEADER, verify=False)
  devise_json           = api_request.json()
  gw_name_list          = []

  for gateway in devise_json["versanms.ApplianceStatusResult"]["appliances"]:
    gw_name_list.append(gateway["name"])

  return gw_name_list

def get_security_rules(target_url, target):
    try:
        api_request     = requests.get(target_url, auth=(args.user, args.password), headers=HTTP_HEADER, verify=False)
        api_response    = api_request.text
        reponse_code    = api_request.status_code
        json_output     = json.loads(api_response)

        return json_output

    except:
        print(GET_POLICY_MESSAGE_ERROR % (target) )

def list_is_shadowed(primary_list,candidate_list):
    if  primary_list == []:    
        return True
    else:
        primary_set     = set(primary_list)
        shadowed_set    = set(candidate_list)
        if candidate_list != []:
            if shadowed_set.issubset(primary_set):
                return True
        else:
            return False

def rule_is_shadowed(primary_rule, candidate_rule):
    
    shadowing_exist                             = True
    primary_source_address_list                 = [] 
    candidate_source_address_list               = [] 
    primary_source_group_address_list           = [] 
    candidate_source_group_address_list         = [] 
    primary_source_zone_list                    = [] 
    candidate_source_zone_list                  = [] 
    primary_destination_address_list            = [] 
    candidate_destination_address_list          = []
    primary_destination_group_address_list      = [] 
    candidate_destination_group_address_list    = [] 
    primary_destination_zone_list               = [] 
    candidate_destination_zone_list             = [] 
    primary_service_list                        = [] 
    candidate_service_list                      = [] 
    primary_application_list                    = [] 
    candidate_application_list                  = [] 

    if primary_rule['rule-disable'] == 'false' and candidate_rule['rule-disable'] == 'false':
        if 'source' in primary_rule['match']:
            if 'address' in primary_rule['match']['source']:
                if 'address-list' in primary_rule['match']['source']['address']:
                    for address_value in primary_rule['match']['source']['address']['address-list']:
                      primary_source_address_list.append(address_value)
                    primary_source_address_list.sort()   
                if 'address-group-list' in primary_rule['match']['source']['address']:
                    for address_group_value in primary_rule['match']['source']['address']['address-group-list']:
                        primary_source_group_address_list.append(address_group_value)
                    primary_source_group_address_list.sort()

            if 'zone' in primary_rule['match']['source']:
                if 'zone-list' in primary_rule['match']['source']['zone']:
                    for zone_value in primary_rule['match']['source']['zone']['zone-list']:
                        primary_source_zone_list.append(zone_value)
                    primary_source_zone_list.sort()

        if 'source' in candidate_rule['match']:
            if 'address' in candidate_rule['match']['source']:
                if 'address-list' in candidate_rule['match']['source']['address']:
                    for address_value in candidate_rule['match']['source']['address']['address-list']:
                        candidate_source_address_list.append(address_value)
                    candidate_source_address_list.sort()  
                if 'address-group-list' in candidate_rule['match']['source']['address']:
                    for address_group_value in candidate_rule['match']['source']['address']['address-group-list']:
                        candidate_source_group_address_list.append(address_group_value)
                    candidate_source_group_address_list.sort()

            if 'zone' in candidate_rule['match']['source']:
                if 'zone-list' in candidate_rule['match']['source']['zone']:
                    for zone_value in candidate_rule['match']['source']['zone']['zone-list']:
                        candidate_source_zone_list.append(zone_value)
                    candidate_source_zone_list.sort()

        if not list_is_shadowed(primary_source_address_list,candidate_source_address_list):
            return False

        if not list_is_shadowed(primary_source_zone_list,candidate_source_zone_list):
            return False
    
        if not list_is_shadowed(primary_source_group_address_list,candidate_source_group_address_list):
            return False

        if 'destination' in primary_rule['match']:
            if 'address' in primary_rule['match']['destination']:
                if 'address-list' in primary_rule['match']['destination']['address']:
                    for address_value in primary_rule['match']['destination']['address']['address-list']:
                        primary_destination_address_list.append(address_value)
                    primary_destination_address_list.sort()  
                if 'address-group-list' in primary_rule['match']['destination']['address']:
                    for address_group_value in primary_rule['match']['destination']['address']['address-group-list']:
                        primary_destination_group_address_list.append(address_group_value)
                    primary_destination_group_address_list.sort()

            if 'zone' in primary_rule['match']['destination']:
                if 'zone-list' in primary_rule['match']['destination']['zone']:
                    for zone_value in primary_rule['match']['destination']['zone']['zone-list']:
                        primary_destination_zone_list.append(zone_value)
                    primary_destination_zone_list.sort() 

        if 'destination' in candidate_rule['match']:
            if 'address' in candidate_rule['match']['destination']:
                if 'address-list' in candidate_rule['match']['destination']['address']:
                    for address_value in candidate_rule['match']['destination']['address']['address-list']:
                        candidate_destination_address_list.append(address_value)
                    candidate_destination_address_list.sort()  
                if 'address-group-list' in candidate_rule['match']['destination']['address']:
                    for address_group_value in candidate_rule['match']['destination']['address']['address-group-list']:
                        candidate_destination_group_address_list.append(address_group_value)
                    candidate_destination_group_address_list.sort()

            if 'zone' in candidate_rule['match']['destination']:
                if 'zone-list' in candidate_rule['match']['destination']['zone']:
                    for zone_value in candidate_rule['match']['destination']['zone']['zone-list']:
                        candidate_destination_zone_list.append(zone_value)
                    candidate_destination_zone_list.sort()

        if not list_is_shadowed(primary_destination_address_list,candidate_destination_address_list):
            return False
    
        if not list_is_shadowed(primary_destination_zone_list,candidate_destination_zone_list):
            return False

        if not list_is_shadowed(primary_destination_group_address_list,candidate_destination_group_address_list):
            return False
    
        if 'services' in primary_rule['match']:
            if 'predefined-services-list' in primary_rule['match']['services']:
                for service_value in primary_rule['match']['services']['predefined-services-list']:
                    primary_service_list.append(service_value)
                primary_service_list.sort()
            if 'services-list' in primary_rule['match']['services']:
                for service_value in primary_rule['match']['services']['services-list']:
                    primary_service_list.append(service_value)
                primary_service_list.sort()

        if 'services' in candidate_rule['match']:
            if 'predefined-services-list' in candidate_rule['match']['services']:
                for service_value in candidate_rule['match']['services']['predefined-services-list']:
                    candidate_service_list.append(service_value)
                candidate_service_list.sort()
            if 'services-list' in candidate_rule['match']['services']:
                for service_value in candidate_rule['match']['services']['services-list']:
                    candidate_service_list.append(service_value)
                candidate_service_list.sort()

        if not list_is_shadowed(primary_service_list,candidate_service_list):
            return False
    
        if 'application' in primary_rule['match']:
            if 'predefined-application-list' in primary_rule['match']['application']:
                for app_value in primary_rule['match']['application']['predefined-application-list']:
                    primary_application_list.append(app_value)
                primary_application_list.sort()
                        
            if 'predefined-group-list' in primary_rule['match']['application']:
                for app_value in primary_rule['match']['application']['predefined-group-list']:
                    primary_application_list.append(app_value)
                primary_application_list.sort()
       
            if 'predefined-filter-list' in primary_rule['match']['application']:
                for app_value in primary_rule['match']['application']['predefined-filter-list']:
                    primary_application_list.append(app_value)
                primary_application_list.sort()

        if 'application' in candidate_rule['match']:
            if 'predefined-application-list' in candidate_rule['match']['application']:
                for app_value in candidate_rule['match']['application']['predefined-application-list']:
                    candidate_application_list.append(app_value)
                candidate_application_list.sort()
                        
            if 'predefined-group-list' in candidate_rule['match']['application']:
                for app_value in candidate_rule['match']['application']['predefined-group-list']:
                    candidate_application_list.append(app_value)
                candidate_application_list.sort()
       
            if 'predefined-filter-list' in candidate_rule['match']['application']:
                for app_value in candidate_rule['match']['application']['predefined-filter-list']:
                    candidate_application_list.append(app_value)
                candidate_application_list.sort()

        if not list_is_shadowed(primary_application_list,candidate_application_list):
            return False

    else:
        return False
      
    return shadowing_exist


if __name__ == '__main__':

    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter, description='Script to reveal shadowed rules on Versa CPE and Templates')
    parser.add_argument('--director', default='director.yourcompany.com', type=str,help='IP address or FQDN of Versa Director')
    parser.add_argument('--target', default='', type=str,help='Device (device:XXX) or Device Template (template:XXX) where the shadowed rules must be reveal.')
    parser.add_argument('--org', default='Versa', type=str,help='Organization name')
    parser.add_argument('--group', default='Default-Policy', type=str,help='Policy Group Name')
    parser.add_argument('--user', default='Administrator', type=str,help='GUI username of Director')
    parser.add_argument('--password', default='password', type=str,help='GUI password of Director')
    parser.add_argument('--action', default='display', type=str,help='Action to be taken on the shadowed rules. Use --action help for details')
    
    args = parser.parse_args()

    if args.action == 'help':
        help_action()

    if re.match('device\:all',args.target):
        all_devices = get_tenant_device_list()

        for device_name in all_devices:

            target_name = device_name
            print(RUN_MESSAGE_HEADER % (device_name))
            try:
                url_all_rules   = URL_GET_DEVICE_ALL_RULES % (args.director, device_name, args.org, args.group)
                device_rules     = get_security_rules(url_all_rules,device_name)

                for primary_rule in device_rules['access-policy']:
                    for candidate_rule in device_rules['access-policy']:
                        if candidate_rule['vSeqNo'] > primary_rule['vSeqNo']:
                            if rule_is_shadowed(primary_rule,candidate_rule):
                                exec_action(target_name, primary_rule,candidate_rule)
            except:
                pass
    
    elif re.match('template\:all',args.target):
        all_templates = get_tenant_dev_template_name_list()

        for template_name in all_templates:

            target_name = template_name
            print(RUN_MESSAGE_HEADER % (template_name))
            try:
                url_all_rules   = URL_GET_DEV_TEMPLATE_ALL_RULES % (args.director, template_name, args.org, args.group)    
                template_rules  = get_security_rules(url_all_rules, template_name)

                for primary_rule in template_rules['access-policy']:
                    for candidate_rule in template_rules['access-policy']:
                        if candidate_rule['vSeqNo'] > primary_rule['vSeqNo']:
                            if rule_is_shadowed(primary_rule,candidate_rule):
                                exec_action(target_name, primary_rule,candidate_rule)
            except:
                pass

    elif re.match('device\:.*',args.target):
        target_name = args.target.split(':',1)[1]
        print(RUN_MESSAGE_HEADER % (target_name))

        url_all_rules   = URL_GET_DEVICE_ALL_RULES % (args.director, target_name, args.org, args.group)

        try:
            url_all_rules   = URL_GET_DEVICE_ALL_RULES % (args.director, target_name, args.org, args.group)
            device_rules     = get_security_rules(url_all_rules, target_name)
        
            for primary_rule in device_rules['access-policy']:
                for candidate_rule in device_rules['access-policy']:
                    if candidate_rule['vSeqNo'] > primary_rule['vSeqNo']:
                        if rule_is_shadowed(primary_rule,candidate_rule):
                            exec_action(target_name, primary_rule,candidate_rule)
        except:
            pass

    elif re.match('template\:.*',args.target):
        target_name = args.target.split(':',1)[1]
        print(RUN_MESSAGE_HEADER % (target_name))

        url_all_rules   = URL_GET_DEV_TEMPLATE_ALL_RULES % (args.director, target_name, args.org, args.group)   

        try:
            url_all_rules   = URL_GET_DEV_TEMPLATE_ALL_RULES % (args.director, target_name, args.org, args.group)    
            template_rules  = get_security_rules(url_all_rules,target_name)

            for primary_rule in template_rules['access-policy']:
                for candidate_rule in template_rules['access-policy']:
                    if candidate_rule['vSeqNo'] > primary_rule['vSeqNo']:
                        if rule_is_shadowed(primary_rule,candidate_rule):
                            exec_action(target_name, primary_rule,candidate_rule)
        except:
            pass


