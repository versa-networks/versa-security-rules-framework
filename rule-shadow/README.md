## What on Earth is Rule Shadowing ?
In the context of firewalls, "Rule Shadowing" occurs when there are conflicting or overlapping rules, but only the first rule that matches a particular packet is executed. 
This means that if multiple rules could potentially apply to the same network traffic, only the first rule encountered in the rule set will take effect. This rule is the "Shadowing Rule"

In a production setup, the firewall checks the packet against each rule in order, and as soon as it finds the first rule that matches the packet's characteristics, it applies that rule and stops further rule processing. Any subsequent rules that might also match the packet are effectively "Shadowed" because they won't be executed.

Detecting and addressing rule shadowing is important for maintaining a clear and predictable security policy. It ensures that the intended rules are applied to network traffic and minimize the risk of unclear and messy firewall rule-set. It also ensures the security of the rule-set by revealing rules that might be overly permissive.

## Purpose of the script
The script will inspect every access rules for a given CPE or Template in Versa Director and search for shadowed rules.
Once it find such rules it will display both the shadowing as well as the shadowed rule. In addition the script can perform the following actions on a shadowed rule.

1) display           : Display the shadowed rule
2) delete            : Delete the shadowed rule
3) disable           : Disable the shadowed rule
4) tag               : Tag the shadowed rule. The TAG value include the shadowed & shadowing rule name

## Installation and Dependencies
You will need python3 as well as differents python package. They can be installed locally with pip3
```
pip3 install json
pip3 install requests
pip3 install urllib3
pip3 install argparse
```

## How does it work ?
Before you get started make sure you have the following information:
1) The IP or FQDN of the Director where the CPE are managed.
2) The Director Administrator login ( Default is Administrator)
3) The Director Administrator password ( Default is password )
4) The name of the CPE or template where the change ( or rule review ) is required.
5) The name of the organization where the CPE is managed
6) The name of the Access Policy group where the rules are located ( Default is Default-Policy )

![ALT](./rules-shadow.png)

One you have the settings above, you can use the script following the instruction in the Example section.

##  Example
I'm putting below a couple of interresting examples to perform shadowing rules discovery on VOS access policies.
   
Spot & display shadowed rules on CPE BRANCH-11.
```
# python3 rules-shadow.py --director director-ip.my-domain.com --user admin --password fakepass --target device:BRANCH-11 --org Versa --action display
```

Spot & delete shadowed rules on CPE BRANCH-11.
```
# python3 rules-shadow.py --director director-ip.my-domain.com --user admin --password fakepass  --target device:BRANCH-11 --org Versa --action delete
```

Tag all the shadowed rules from the Template Template-FW-Versa in your Organization .
```
#python3 rules-shadow.py --director director-ip.my-domain.com --user admin --password fakepass  --target template:Template-FW-Versa --org Versa  --action tag 
```

Display all shadowed rules from all Templates in your Organization .
```
#python3 rules-shadow.py --director director-ip.my-domain.com --user admin --password fakepass  --target template:all --org Versa  --action delete
```

## How to use the Help command
You can execute the script with the help flag to get guidance with the syntax.
```
% python3 rules-shadow.py --help                                                                                                                                
usage: rules-shadow.py [-h] [--director DIRECTOR] [--target TARGET] [--org ORG] [--group GROUP] [--user USER] [--password PASSWORD] [--action ACTION]

Script to reveal shadowed rules on Versa CPE and Templates

optional arguments:
  -h, --help           show this help message and exit
  --director DIRECTOR  IP address or FQDN of Versa Director (default: director.yourcompany.com)
  --target TARGET      Device (device:XXX) or Device Template (template:XXX) where the shadowed rules must be reveal. (default: )
  --org ORG            Organization name (default: Versa)
  --group GROUP        Policy Group Name (default: Default-Policy)
  --user USER          GUI username of Director (default: Administrator)
  --password PASSWORD  GUI password of Director (default: password)
  --action ACTION      Action to be taken on the shadowed rules. Use --action help for details (default: display)

% python3 rules-shadow.py --action help

display           : Display the shadowed rules
delete            : Delete the shadowed rules
disable           : Disable the shadowed rules
tag               : Tag the shadowed rule. The TAG value include the shadowing rule name
```